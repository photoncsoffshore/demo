'use strict';

angular.module('demoApp')
    .factory('SearchService', ['$http', function($http){
        //var baseUrl = "http://localhost";
		var baseUrl = "http://172.16.99.24:5000";

        return {
            search: function(data, success, error) {
				alert(baseUrl + '/files/search?file='+data.searchstr);
                $http.get(baseUrl + '/files/search?file='+data.searchstr, data).success(success).error(error)
            }
        };
    }
]);