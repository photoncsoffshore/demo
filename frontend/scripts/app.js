'use strict';

angular.module('demoApp', [
    'ngRoute'
])
.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.
        when('/', {
            templateUrl: 'partials/signin.html',
            controller: 'LoginCtrl'
        }).
        when('/search', {
            templateUrl: 'partials/search.html',
            controller: 'SearchCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });

    }
]);