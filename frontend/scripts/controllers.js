'use strict';

angular.module('demoApp')
    .controller('SearchCtrl', ['$rootScope', '$scope', '$location', 'SearchService', function($rootScope, $scope, $location, SearchService) {

	$scope.search = function() {
		var searchKey = {
			searchstr: $scope.searchstr
		}
		
		SearchService.search(searchKey, function(res) {
			if (res.type == false) {
				alert(res.data)    
			} else {
				window.location = "/";    
			}
		}, function() {
			$rootScope.error = 'Request Failed';
		})
	};
	
}]);
